package logger

import (
	log "github.com/sirupsen/logrus"
)

type LogrusLogger struct {
}

func (l LogrusLogger) Debug(msg string, args ...interface{}) {
	log.Debug(append([]interface{}{msg}, args...))
}

func (l LogrusLogger) Warn(msg string, args ...interface{}) {
	log.Warn(append([]interface{}{msg}, args...))
}

func (l LogrusLogger) Error(msg string, err error, args ...interface{}) {
	log.Error(append([]interface{}{msg, err}, args...))
}

func (l LogrusLogger) Fatal(msg string, err error, args ...interface{}) {
	log.Fatal(append([]interface{}{msg, err}, args...))
}

func (l LogrusLogger) Info(msg string, args ...interface{}) {
	log.Info(append([]interface{}{msg}, args...))
}

func NewLogrusProvider() Logger {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)
	return &LogrusLogger{}
}

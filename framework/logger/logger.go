package logger

type Logger interface {
	Debug(msg string, args ...interface{})
	Warn(msg string, args ...interface{})
	Error(msg string, err error, args ...interface{})
	Fatal(msg string, err error, args ...interface{})
	Info(msg string, args ...interface{})
}

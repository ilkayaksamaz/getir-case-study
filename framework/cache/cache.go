package cache

type Cache interface {
	GetValueById(key string) (string, error)
	Insert(key, val string) error
}

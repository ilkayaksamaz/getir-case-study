package inmemory

import (
	"errors"
	"sync"

	"bitbucket.org/getir-case-study/framework/cache"
)

type InMemoryDb struct {
	db    map[string]string
	mutex sync.RWMutex
}

func NewInmemoryDb() cache.Cache {
	return &InMemoryDb{db: make(map[string]string), mutex: sync.RWMutex{}}
}

func (m *InMemoryDb) GetValueById(key string) (string, error) {
	value, ok := m.db[key]
	if !ok {
		return "", errors.New("key not found")
	}

	return value, nil
}
func (m *InMemoryDb) Insert(key, val string) error {
	m.mutex.Lock()
	m.db[key] = val
	m.mutex.Unlock()
	return nil
}

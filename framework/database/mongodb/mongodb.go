package mongodb

import (
	"context"

	"bitbucket.org/getir-case-study/framework/database"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoDb struct {
	collection *mongo.Collection
}

func NewMongoDb(connection, database, collection string) (database.Database, error) {
	context := context.Background()
	client, err := mongo.Connect(context, options.Client().ApplyURI(connection))

	if err != nil {
		return nil, err
	}

	err = client.Ping(context, readpref.PrimaryPreferred())
	if err != nil {
		return nil, err
	}
	return &MongoDb{client.Database(database).Collection(collection)}, nil
}
func (m *MongoDb) GetAllByFilter(context context.Context, pipeline interface{}, destination interface{}) error {
	currsor, err := m.collection.Aggregate(context, pipeline)
	if err != nil {

		return err
	}
	return currsor.All(context, destination)
}

package database

import "context"

type Database interface {
	GetAllByFilter(context context.Context, pipeline interface{}, destination interface{}) error
}

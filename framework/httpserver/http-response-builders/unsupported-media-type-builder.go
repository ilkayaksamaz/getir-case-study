package httpresponsebuilders

import (
	"net/http"
)

type UnSupportedMediaTypeBuilder struct{}

func NewUnSupportedMediaTypeBuilder() *UnSupportedMediaTypeBuilder {
	return &UnSupportedMediaTypeBuilder{}
}
func (b *UnSupportedMediaTypeBuilder) Build(w http.ResponseWriter, data interface{}, err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(405)
}

package httpresponsebuilders

import (
	"encoding/json"
	"net/http"
)

type OkResponseBuilder struct {
}

func NewOkResponseBuilder() *OkResponseBuilder {
	return &OkResponseBuilder{}
}
func (b *OkResponseBuilder) Build(w http.ResponseWriter, data interface{}, err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	if data != nil {
		js, err := json.Marshal(data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(js)
	}
}

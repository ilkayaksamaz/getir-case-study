package httpresponsebuilders

import (
	"net/http"
)

type InternalServerErrorBuilder struct {
	rp http.ResponseWriter
}

func NewInternalServerErrorBuilder() *InternalServerErrorBuilder {
	return &InternalServerErrorBuilder{}
}
func (b *InternalServerErrorBuilder) Build(w http.ResponseWriter, data interface{}, err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(500)
}

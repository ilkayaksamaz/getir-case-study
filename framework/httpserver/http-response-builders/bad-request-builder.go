package httpresponsebuilders

import (
	"encoding/json"
	"net/http"
)

type BadRequestResponseBuilder struct {
}

func NewBadRequestResponseBuilder() *BadRequestResponseBuilder {
	return &BadRequestResponseBuilder{}
}
func (b *BadRequestResponseBuilder) Build(w http.ResponseWriter, data interface{}, err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(400)
	if data != nil {
		js, err := json.Marshal(data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(js)
	}
}

package httpresponsebuilders

import (
	"net/http"

	"bitbucket.org/getir-case-study/framework/logger"
)

type HttpReponseBuilders interface {
	Build(w http.ResponseWriter, data interface{}, err error)
}

type HttpResponseBuilder struct {
	logger   logger.Logger
	builders map[int]HttpReponseBuilders
}

func NewHttpResponseBuilders(logger logger.Logger) *HttpResponseBuilder {
	return &HttpResponseBuilder{
		logger: logger,
		builders: map[int]HttpReponseBuilders{
			200: NewOkResponseBuilder(),
			400: NewBadRequestResponseBuilder(),
			500: NewInternalServerErrorBuilder(),
			405: NewUnSupportedMediaTypeBuilder(),
		},
	}
}

func (b *HttpResponseBuilder) Build(statusCode int, w http.ResponseWriter, data interface{}, err error) {
	if c, ok := b.builders[statusCode]; ok {
		c.Build(w, data, err)
	} else {
		b.logger.Warn("builder not found")
	}
}

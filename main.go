package main

import (
	"bitbucket.org/getir-case-study/framework/database/mongodb"
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/getir-case-study/api"
	inmemory "bitbucket.org/getir-case-study/framework/cache/in-memory"
	httpresponsebuilders "bitbucket.org/getir-case-study/framework/httpserver/http-response-builders"
	"bitbucket.org/getir-case-study/framework/logger"
	usecases "bitbucket.org/getir-case-study/pkg/application/use-cases"
	repository_impl "bitbucket.org/getir-case-study/pkg/infrastructure/repositories"
)

func elapsed(logger logger.Logger, what string) func() {
	start := time.Now()
	return func() {
		logger.Debug("took", what, time.Since(start))
	}
}

func requestTime(next http.Handler, logger logger.Logger) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer elapsed(logger, r.URL.Path)()
		next.ServeHTTP(w, r)
	})
}

func main() {
	port := os.Getenv("PORT")
	conn := "mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getircase-study?retryWrites=true"
	logger := logger.NewLogrusProvider()

	db, err := mongodb.NewMongoDb(conn, "getir-case-study", "records")
	if err != nil {
		logger.Fatal("can't connect mongodb", err)
		panic(err)
	}
	recordRepository := repository_impl.NewRecordRepository(db)
	keyValueRepository := repository_impl.NewKeyValueRepository(inmemory.NewInmemoryDb())
	recordController := api.NewRecordController(usecases.NewRecordsUseCase(recordRepository), httpresponsebuilders.NewHttpResponseBuilders(logger), logger)
	keyValueController := api.NewKeyValueController(usecases.NewKeyValueUseCase(keyValueRepository), httpresponsebuilders.NewHttpResponseBuilders(logger), logger)

	http.Handle("/records", requestTime(recordController.Handler(), logger))
	http.Handle("/in-memory", requestTime(keyValueController.Handler(), logger))

	httpServer := &http.Server{
		Addr: ":" + port,
	}

	go func() {
		logger.Debug("server started")

		if err := httpServer.ListenAndServe(); err != http.ErrServerClosed {
			logger.Fatal("HTTP server ListenAndServe", err)
		}
	}()

	signalChan := make(chan os.Signal, 1)

	signal.Notify(
		signalChan,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGQUIT,
	)

	<-signalChan
	logger.Debug("os.Interrupt - shutting down...")

	go func() {
		<-signalChan
		logger.Debug("os.Kill - terminating...")
	}()

	gracefullCtx, cancelShutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelShutdown()

	if err := httpServer.Shutdown(gracefullCtx); err != nil {
		logger.Error("shutdown error: ", err)
		defer os.Exit(1)
		return
	} else {
		logger.Debug("gracefully stopped")
	}

	defer os.Exit(0)

}

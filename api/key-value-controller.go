package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	httpresponsebuilders "bitbucket.org/getir-case-study/framework/httpserver/http-response-builders"
	"bitbucket.org/getir-case-study/framework/logger"
	usecases "bitbucket.org/getir-case-study/pkg/application/use-cases"
)

type KeyValueController struct {
	useCase         *usecases.GetKeyValueUseCase
	responseBuilder *httpresponsebuilders.HttpResponseBuilder
	logger          logger.Logger
}

func NewKeyValueController(useCase *usecases.GetKeyValueUseCase, responseBuilder *httpresponsebuilders.HttpResponseBuilder, logger logger.Logger) *KeyValueController {
	return &KeyValueController{useCase, responseBuilder, logger}
}

func (c *KeyValueController) Handler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			c.Get(w, r)
			return
		case "POST":
			c.Post(w, r)
			return
		default:
			c.logger.Warn("unsupported media type", r.URL.Path)
			c.responseBuilder.Build(405, w, nil, nil)
		}
	})
}
func (c *KeyValueController) Post(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	parameters := new(usecases.InsertKeyValueDataRequest)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		c.logger.Error("can't read body", err, r.URL.Path)
		c.responseBuilder.Build(400, w, nil, nil)
		return
	}
	if len(body) > 0 {
		err = json.Unmarshal(body, parameters)
		if err != nil {
			c.logger.Error("unexpected json format", err, string(body))
			c.responseBuilder.Build(400, w, nil, nil)
			return
		}
	}

	err = c.useCase.Insert(parameters)
	if err != nil {
		c.responseBuilder.Build(400, w, nil, nil)
		return
	}
	c.responseBuilder.Build(200, w, nil, nil)
}
func (c *KeyValueController) Get(w http.ResponseWriter, r *http.Request) {
	key := r.URL.Query().Get("key")
	if key == "" {
		c.responseBuilder.Build(400, w, "key must provide", nil)
		return
	}

	val, err := c.useCase.GetValueByKey(key)
	if err != nil {
		c.logger.Error("unexpected err", err)
		c.responseBuilder.Build(400, w, "not found", nil)
	}
	c.responseBuilder.Build(200, w, val, nil)
}

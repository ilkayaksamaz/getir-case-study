package api

import (
	"bitbucket.org/getir-case-study/pkg/domain/contracts"
)

type RecordApiResponse struct {
	Code    int                         `json:"code"`
	Message string                      `json:"msg"`
	Records []*contracts.RecordContract `json:"records"`
}

func NewRecordApiResponse(code int, message string, records []*contracts.RecordContract) *RecordApiResponse {
	return &RecordApiResponse{code, message, records}
}

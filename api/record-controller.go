package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	httpresponsebuilders "bitbucket.org/getir-case-study/framework/httpserver/http-response-builders"
	"bitbucket.org/getir-case-study/framework/logger"
	usecases "bitbucket.org/getir-case-study/pkg/application/use-cases"
)

type RecordController struct {
	useCase         *usecases.GetRecordsUseCase
	responseBuilder *httpresponsebuilders.HttpResponseBuilder
	logger          logger.Logger
}

func NewRecordController(useCase *usecases.GetRecordsUseCase, responseBuilder *httpresponsebuilders.HttpResponseBuilder, logger logger.Logger) *RecordController {
	return &RecordController{useCase, responseBuilder, logger}
}

func (c *RecordController) Handler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			c.Get(w, r)
			return
		default:
			c.logger.Warn("unsupported media type", r.URL.Path)
			c.responseBuilder.Build(405, w, nil, nil)
		}
	})
}
func (c *RecordController) Get(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	parameters := new(usecases.GetRecordRequest)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		c.logger.Error("can't read body", err, r.URL.Path)
		c.responseBuilder.Build(400, w, NewRecordApiResponse(400, "BadRequest", nil), nil)
		return
	}
	if len(body) > 0 {
		err = json.Unmarshal(body, parameters)
		if err != nil {
			c.logger.Error("unexpected json format", err, string(body))
			c.responseBuilder.Build(400, w, NewRecordApiResponse(400, "BadRequest", nil), nil)
			return
		}
	}
	result, err := c.useCase.GetRecords(parameters)
	if err != nil {
		c.logger.Error("unexpected err", err)
		c.responseBuilder.Build(500, w, NewRecordApiResponse(500, "InternalServerError", nil), nil)
		return
	}
	c.responseBuilder.Build(200, w, NewRecordApiResponse(200, "Success", result), nil)
}

package repositories

import "bitbucket.org/getir-case-study/pkg/domain/entities"

type KeyValueRepository interface {
	Get(key string) (*entities.KeyValueEntity, error)
	Insert(entity *entities.KeyValueEntity) error
}

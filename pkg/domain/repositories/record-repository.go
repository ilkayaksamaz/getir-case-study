package repositories

import (
	"time"

	"bitbucket.org/getir-case-study/pkg/domain/entities"
)

type RecordRepository interface {
	Get(startDate, endDate *time.Time, minCount, maxCount int) ([]*entities.RecordEntity, error)
}

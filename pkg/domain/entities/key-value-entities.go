package entities

type KeyValueEntity struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func NewKeyValueEntity(key, value string) *KeyValueEntity {
	return &KeyValueEntity{key, value}
}
func (k *KeyValueEntity) GetKey() string {
	return k.Key
}
func (k *KeyValueEntity) GetValue() string {
	return k.Value
}

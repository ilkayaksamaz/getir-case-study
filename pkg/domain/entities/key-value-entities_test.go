package entities

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestKeyValueEntity_GetKey(t *testing.T) {
	//arrange
	key := "sample"
	value := "value"
	entity := NewKeyValueEntity(key, value)

	//act
	getKeyResult := entity.GetKey()
	//assert
	assert.Equal(t, getKeyResult, key)
}

func TestKeyValueEntity_GetValue(t *testing.T) {
	//arrange
	key := "sample"
	value := "value"
	entity := NewKeyValueEntity(key, value)

	//act
	getValueResult := entity.GetValue()
	//assert
	assert.Equal(t, getValueResult, value)
}

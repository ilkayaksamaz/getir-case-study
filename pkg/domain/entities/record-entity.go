package entities

import (
	"time"
)

type RecordEntity struct {
	Key        string     `bson:"key"`
	CreatedAt  *time.Time `bson:"createdAt"`
	Value      string     `bson:"value"`
	TotalCount int        `bson:"totalCount"`
}

func (r *RecordEntity) GetKey() string {
	return r.Key
}

func (r *RecordEntity) GetCreatedAt() *time.Time {
	return r.CreatedAt
}

func (r *RecordEntity) GetValue() string {
	return r.Value
}

func (r *RecordEntity) GetTotalCount() int {
	return r.TotalCount
}

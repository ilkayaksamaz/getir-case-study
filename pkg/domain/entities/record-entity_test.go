package entities

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestRecordEntity_GetCreatedAt(t *testing.T) {
	//arrange
	key, createdAt, value, totalCount := "sample", time.Now(), "value", 100
	entity := &RecordEntity{key, &createdAt, value, totalCount}
	//act
	getCreatedAtResult := entity.GetCreatedAt()
	//assert
	assert.Equal(t, createdAt, getCreatedAtResult)
}

func TestRecordEntity_GetKey(t *testing.T) {
	//arrange
	key, createdAt, value, totalCount := "sample", time.Now(), "value", 100
	entity := &RecordEntity{key, &createdAt, value, totalCount}
	//act
	getKeyResult := entity.GetKey()
	//assert
	assert.Equal(t, key, getKeyResult)
}

func TestRecordEntity_GetValue(t *testing.T) {
	//arrange
	key, createdAt, value, totalCount := "sample", time.Now(), "value", 100
	entity := &RecordEntity{key, &createdAt, value, totalCount}
	//act
	getValueResult := entity.GetValue()
	//assert
	assert.Equal(t, value, getValueResult)
}

func TestRecordEntity_GetTotalCount(t *testing.T) {
	//arrange
	key, createdAt, value, totalCount := "sample", time.Now(), "value", 100
	entity := &RecordEntity{key, &createdAt, value, totalCount}
	//act
	getTotalCountResult := entity.GetTotalCount()
	//assert
	assert.Equal(t, totalCount, getTotalCountResult)
}

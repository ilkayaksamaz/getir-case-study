package assemblers

import (
	"bitbucket.org/getir-case-study/pkg/domain/contracts"
	"bitbucket.org/getir-case-study/pkg/domain/entities"
)

func ToKeyValueContract(entity *entities.KeyValueEntity) *contracts.KeyValueContract {
	return &contracts.KeyValueContract{
		Key:   entity.GetKey(),
		Value: entity.GetValue(),
	}
}

package assemblers

import (
	"bitbucket.org/getir-case-study/pkg/domain/contracts"
	"bitbucket.org/getir-case-study/pkg/domain/entities"
)

func ToRecordContract(entity *entities.RecordEntity) *contracts.RecordContract {
	return &contracts.RecordContract{
		Key:        entity.GetKey(),
		CreatedAt:  entity.GetCreatedAt(),
		Value:      entity.GetValue(),
		TotalCount: entity.GetTotalCount()}
}

func ToRecordContracts(entities []*entities.RecordEntity) []*contracts.RecordContract {
	result := make([]*contracts.RecordContract, 0)
	for _, v := range entities {
		result = append(result, ToRecordContract(v))
	}
	return result
}

package contracts

type KeyValueContract struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

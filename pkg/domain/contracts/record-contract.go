package contracts

import "time"

type RecordContract struct {
	Key        string     `json:"key"`
	CreatedAt  *time.Time `json:"createdAt"`
	Value      string     `json:"value"`
	TotalCount int        `json:"totalCount"`
}

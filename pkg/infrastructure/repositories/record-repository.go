package repository_impl

import (
	"context"
	"time"

	"bitbucket.org/getir-case-study/framework/database"
	"bitbucket.org/getir-case-study/pkg/domain/entities"
	"bitbucket.org/getir-case-study/pkg/domain/repositories"
)

type RecordRepository struct {
	connection database.Database
}

func NewRecordRepository(connection database.Database) repositories.RecordRepository {
	return &RecordRepository{connection}
}

func (r *RecordRepository) Get(startDate, endDate *time.Time, minCount, maxCount int) ([]*entities.RecordEntity, error) {
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*20)
	defer cancelFunc()
	parameters := NewRecordParameterBuilder().
		WithStartDate(startDate).
		WithEndDate(endDate).
		WithMaxCount(maxCount).
		WithMinCount(minCount).
		Build()

	projection := map[string]interface{}{"$project": map[string]interface{}{
		"key":        1,
		"createdAt":  1,
		"value":      1,
		"totalCount": map[string]interface{}{"$sum": "$counts"}},
	}
	match := map[string]interface{}{"$match": parameters}
	pipelineItems := []interface{}{projection, match}

	var destination []*entities.RecordEntity
	err := r.connection.GetAllByFilter(ctx, pipelineItems, &destination)
	if err != nil {
		return nil, err
	}
	return destination, nil
}

package repository_impl

import "time"

type recordsParameterBuilder struct {
	whereClause map[string]map[string]interface{}
}

func NewRecordParameterBuilder() *recordsParameterBuilder {
	return &recordsParameterBuilder{whereClause: make(map[string]map[string]interface{})}
}

func (r *recordsParameterBuilder) WithStartDate(date *time.Time) *recordsParameterBuilder {
	if date != nil {
		if r.whereClause["createdAt"] == nil {
			r.whereClause["createdAt"] = make(map[string]interface{})
		}
		t := time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, time.UTC)
		r.whereClause["createdAt"]["$gte"] = t
	}
	return r
}

func (r *recordsParameterBuilder) WithEndDate(date *time.Time) *recordsParameterBuilder {
	if date != nil {
		if r.whereClause["createdAt"] == nil {
			r.whereClause["createdAt"] = make(map[string]interface{})
		}
		t := time.Date(date.Year(), date.Month(), date.Day(), 23, 59, 59, 59, time.UTC)
		r.whereClause["createdAt"]["$lte"] = t
	}
	return r
}

func (r *recordsParameterBuilder) WithMaxCount(count int) *recordsParameterBuilder {
	if count > 0 {
		if r.whereClause["totalCount"] == nil {
			r.whereClause["totalCount"] = make(map[string]interface{})
		}
		r.whereClause["totalCount"]["$lte"] = count
	}
	return r
}

func (r *recordsParameterBuilder) WithMinCount(count int) *recordsParameterBuilder {
	if count > 0 {
		if r.whereClause["totalCount"] == nil {
			r.whereClause["totalCount"] = make(map[string]interface{})
		}
		r.whereClause["totalCount"]["$gte"] = count
	}
	return r
}

func (r *recordsParameterBuilder) Build() map[string]map[string]interface{} {
	return r.whereClause
}

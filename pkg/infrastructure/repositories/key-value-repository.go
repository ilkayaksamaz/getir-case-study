package repository_impl

import (
	"bitbucket.org/getir-case-study/framework/cache"
	"bitbucket.org/getir-case-study/pkg/domain/entities"
	"bitbucket.org/getir-case-study/pkg/domain/repositories"
)

type KeyValueRepository struct {
	cache cache.Cache
}

func NewKeyValueRepository(cache cache.Cache) repositories.KeyValueRepository {
	return &KeyValueRepository{cache}
}

func (k *KeyValueRepository) Get(key string) (*entities.KeyValueEntity, error) {
	val, err := k.cache.GetValueById(key)
	if err != nil {
		return nil, err
	}
	return entities.NewKeyValueEntity(key, val), nil
}
func (k *KeyValueRepository) Insert(entity *entities.KeyValueEntity) error {
	err := k.cache.Insert(entity.Key, entity.Value)
	if err != nil {
		return err
	}
	return nil
}

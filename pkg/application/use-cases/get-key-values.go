package usecases

import (
	"bitbucket.org/getir-case-study/pkg/domain/assemblers"
	"bitbucket.org/getir-case-study/pkg/domain/contracts"
	"bitbucket.org/getir-case-study/pkg/domain/entities"
	"bitbucket.org/getir-case-study/pkg/domain/repositories"
)

type GetKeyValueUseCase struct {
	repository repositories.KeyValueRepository
}

type InsertKeyValueDataRequest struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func NewKeyValueUseCase(repository repositories.KeyValueRepository) *GetKeyValueUseCase {
	return &GetKeyValueUseCase{repository}
}

func (k *GetKeyValueUseCase) GetValueByKey(key string) (*contracts.KeyValueContract, error) {
	val, err := k.repository.Get(key)
	if err != nil {
		return nil, err
	}
	return assemblers.ToKeyValueContract(val), nil
}

func (k *GetKeyValueUseCase) Insert(request *InsertKeyValueDataRequest) error {
	err := k.repository.Insert(entities.NewKeyValueEntity(request.Key, request.Value))
	if err != nil {
		return err
	}
	return nil
}

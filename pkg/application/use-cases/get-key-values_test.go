package usecases

import (
	mocks "bitbucket.org/getir-case-study/mocks/pkg/domain/repositories"
	"bitbucket.org/getir-case-study/pkg/domain/entities"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetKeyValueUseCase_GetValueByKey(t *testing.T) {
	//arrange
	key := "key"
	value := "value"
	repositoryResult := entities.NewKeyValueEntity(key, value)
	repository := &mocks.KeyValueRepository{}
	repository.On("Get", key).Return(repositoryResult, nil)
	u := NewKeyValueUseCase(repository)
	//act
	byKey, err := u.GetValueByKey(key)
	assert.Empty(t, err)
	assert.Equal(t, byKey.Key, key)
}

func TestGetKeyValueUseCase_Insert(t *testing.T) {
	//arrange
	key := "key"
	value := "value"
	repositoryResult := entities.NewKeyValueEntity(key, value)
	repository := &mocks.KeyValueRepository{}
	repository.On("Insert", repositoryResult).Return(nil)
	u := NewKeyValueUseCase(repository)
	//act
	err := u.Insert(&InsertKeyValueDataRequest{
		Key:   key,
		Value: value,
	})
	//assert
	assert.Empty(t, err)
}

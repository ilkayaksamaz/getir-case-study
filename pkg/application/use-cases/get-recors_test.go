package usecases

import (
	mocks "bitbucket.org/getir-case-study/mocks/pkg/domain/repositories"
	"bitbucket.org/getir-case-study/pkg/domain/entities"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestGetRecordsUseCase_GetRecords(t *testing.T) {
	now := time.Date(2021, 10, 11, 00, 00, 00, 00, time.UTC)
	//arrange
	startDate, endDate := now, now
	startStr, endStr, minCount, maxCount := startDate.Format("2006-01-02"), endDate.Format("2006-01-02"), 100, 1000
	request := &GetRecordRequest{startStr, endStr, minCount, maxCount}
	repository := &mocks.RecordRepository{}
	results := make([]*entities.RecordEntity, 0)
	results = append(results, &entities.RecordEntity{})
	repository.On("Get", &startDate, &endDate, minCount, maxCount).Return(results, nil)
	u := NewRecordsUseCase(repository)
	//act
	records, err := u.GetRecords(request)

	//assert
	assert.Empty(t, err)
	assert.Equal(t, len(records), 1)
}

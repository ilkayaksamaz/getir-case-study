package usecases

import (
	"time"

	"bitbucket.org/getir-case-study/pkg/domain/assemblers"
	"bitbucket.org/getir-case-study/pkg/domain/contracts"
	"bitbucket.org/getir-case-study/pkg/domain/repositories"
)

type GetRecordRequest struct {
	StartDate string `json:"startDate"`
	EndDate   string `json:"endDate"`
	MinCount  int    `json:"minCount"`
	MaxCount  int    `json:"maxCount"`
}

type GetRecordsUseCase struct {
	repository repositories.RecordRepository
}

func NewRecordsUseCase(repository repositories.RecordRepository) *GetRecordsUseCase {
	return &GetRecordsUseCase{repository: repository}
}

func (g *GetRecordsUseCase) GetRecords(request *GetRecordRequest) ([]*contracts.RecordContract, error) {
	layout := "2006-01-02"
	var startDate *time.Time = nil
	if start, err := time.ParseInLocation(layout, request.StartDate, time.UTC); err == nil {
		startDate = &start
	}
	var endDate *time.Time = nil
	if end, err := time.ParseInLocation(layout, request.EndDate, time.UTC); err == nil {
		endDate = &end
	}

	records, err := g.repository.Get(startDate, endDate, request.MinCount, request.MaxCount)
	if err != nil {
		return nil, err
	}
	return assemblers.ToRecordContracts(records), nil
}

module bitbucket.org/getir-case-study

go 1.16

require (
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.7.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
